package ctr;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import ejb.TenantEjb;
import ejb.UserEjb;
import entity.Tenant;
import entity.User;
import utils.Token;

@ManagedBean
@SessionScoped
public class Identity {
	
	User user=new User();
	Token token=null;
	Tenant tenant=new Tenant();
	
	@EJB
	UserEjb userEjb;
	@EJB
	TenantEjb tenantEjb;
	
	public String doLogOut() {
		
		this.token=null;
		
		return "/login?faces-redirect=true";
	}
	
	
	public String doLogin() {
		this.token=userEjb.logIn(user);
		if(token.getToken() == null) {
			this.token=null;
			return "./";
		}
		
		this.doGetUserFromToken();
		this.doGetTenant();
		return "/dashboard?faces-redirect=true";
	}
	
	public void doGetUserFromToken() {
		this.user=userEjb.getByToken(this.token);
		System.out.println("[Identity] User Connected "+this.user.getUsername());
	}
	
	public void doGetTenant() {
		this.tenant=tenantEjb.getById(this.getUser().getTenantId(),"Bearer "+this.getToken().getToken());
	}

	
	

	public User getUser() {
		return user;
	}


	public void setUser(User user) {
		this.user = user;
	}


	public Token getToken() {
		return token;
	}


	public void setToken(Token token) {
		this.token = token;
	}


	public Tenant getTenant() {
		return tenant;
	}


	public void setTenant(Tenant tenant) {
		this.tenant = tenant;
	}
	
	
	
	

}
