package ctr;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.servlet.http.Part;

import ejb.UserEjb;
import entity.User;




@ManagedBean
@SessionScoped
public class Userctr{
	
	@ManagedProperty(value="#{identity}")
	Identity identity;
	
	
	
	public Identity getIdentity() {
		return identity;
	}

	public void setIdentity(Identity identity) {
		this.identity = identity;
	}

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	@PostConstruct
	private void init() {
		this.user=identity.getUser();
	}
	
	public Part file;
	public Part getFile() {
		return file;
	}

	public void setFile(Part file) {
		this.file = file;
	}
	User user ;
	
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	@EJB
	UserEjb userejb ;
	
	List<User> users ;
	

	public String doAdd( ){
		try {

			if (file.getContentType().contains("png")) {
				InputStream input = file.getInputStream();
				OutputStream output = new FileOutputStream(new File("C:\\safa\\uploads\\" + user.getName() + ".png"));
				byte[] buf = new byte[1024];
				int len;
				while ((len = input.read(buf)) > 0) {
					output.write(buf, 0, len);
				}
				
				input.close();
				output.close();
				user.setProfileImageURL("C:\\safa\\uploads\\" + user.getName() + ".png");
			}

			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		userejb.AddUser(user,"Bearer "+identity.getToken().getToken());
		user = new User();
		return "/login?faces-redirect=true";

		// basicOpsLocal.ajouterUser(user);
		// user = new User() ;

	}
	
	public String doupdate() {
		try {

			if (file.getContentType().contains("png")) {
				InputStream input = file.getInputStream();
				OutputStream output = new FileOutputStream(new File("C:\\safa\\uploads\\" + user.getName() + ".png"));
				byte[] buf = new byte[1024];
				int len;
				while ((len = input.read(buf)) > 0) {
					output.write(buf, 0, len);
				}
				// IOUtils.copy(input, output) ;
				input.close();
				output.close();
				user.setProfileImageURL("C:\\safa\\uploads\\" + user.getName() + ".png");
			}

			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		userejb.update(user,this.user.getId(),"Bearer "+identity.getToken().getToken());
		return "/login?faces-redirect=true\";";
	}
	
	public String doGoToUpdate() {
		return "/views/user/update?faces-redirect=true";
	}
}