package ctr;

import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.persistence.PostLoad;
import javax.persistence.PostUpdate;
import javax.servlet.http.HttpServletResponse;

import com.lowagie.text.Document;
import com.lowagie.text.PageSize;
import com.lowagie.text.pdf.PdfWriter;

import ejb.EventEjb;
import entity.Event;
import entity.EventComment;
import entity.EventParticipation;
import utils.QRCodeCreator;

@ManagedBean
@SessionScoped
public class EventBean {
	@EJB
	EventEjb eventEJB;
	
	@ManagedProperty(value="#{identity}")
	Identity identity;
	
	List<Event> events;
	Event event=new Event();
	String searchString="";
	String comment;
	
	
	
	

	public String getComment() {
		return comment;
	}



	public void setComment(String comment) {
		this.comment = comment;
	}



	@PostConstruct
	private void init() {
		System.out.println("**************init eventbean ***********");
		this.doGetAll();
	}
	

public String doPostComment() {
	
	System.out.println("********* adding comment");
	EventComment ec=new EventComment();
	ec.setContent(this.comment);
	ec.setEventId(this.event.getId());
	ec.setUserId(identity.getUser().getId());
	eventEJB.addComment(ec,"Bearer "+identity.getToken().getToken());
	
	this.comment="";
	return this.doGetEvent(this.event.getId());
}
	
	
public String doGetEvent(int id) {
	System.out.println("******  getbyid  ");
	this.event = eventEJB.getById(id,"Bearer "+identity.getToken().getToken());
	System.out.println("retirned event: "+event.getId());
	return "/views/event/details?faces-redirect=true";
}

public void doGetAll() {
	System.out.println("identity "+this.identity.getUser().getUsername());
	System.out.println("******  get all  ");
	this.events=eventEJB.getAll("Bearer "+identity.getToken().getToken());
}

public List<Event> getEvents() {
	return events;
}

public void setEvents(List<Event> events) {
	this.events = events;
}



public Event getEvent() {
	return event;
}


public void setEvent(Event event) {
	this.event = event;
}	



public Identity getIdentity() {
	return identity;
}


public void setIdentity(Identity identity) {
	this.identity = identity;
}




public String getSearchString() {
	return searchString;
}



public void setSearchString(String searchString) {
	this.searchString = searchString;
}

public String doGetBySearchString(){
	System.out.println("searchString: "+searchString);
	this.events=eventEJB.getBySearchString(searchString, "Bearer "+identity.getToken().getToken());
	return "/views/event/index?faces-redirect=true";
	
}



public boolean isNotParticipating() {
	for(EventParticipation e : this.event.getParticipatingUsers()) {
		//change 1 by the id of the user in the session
		if(e.getUserId()==identity.getUser().getId())
			return false;
	}
	return true;
}

public String doParticipate() {
	System.out.println("subscribing *****************");
	this.eventEJB.subscribe(this.event.getId(),identity.getUser().getEmail(),"Bearer "+identity.getToken().getToken());
	doGetEvent(this.event.getId());
	return "/views/event/details?faces-redirect=true";
}

public void doDownloadPass() throws IOException {
	   String arg="http://pi-net-servicepattern-dev.eu-west-1.elasticbeanstalk.com/api/ep/validate/";
	   
	   for(EventParticipation p:this.event.getParticipatingUsers()) {
		   if(p.getUserId()==this.identity.getUser().getId())
			   arg+=p.getActivationKey();
	   }
    
    FacesContext facesContext = FacesContext.getCurrentInstance();

    HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();

    response.reset();
    response.setHeader("Content-Type", "application/octet-stream");
    response.setHeader("Content-Disposition", "attachment;filename=file.pdf");
    OutputStream responseOutputStream = response.getOutputStream();
    try {
		Document document = new Document(PageSize.A4, 50, 50, 50, 50);
		PdfWriter writer = PdfWriter.getInstance(document,responseOutputStream);
		document.open();
		com.lowagie.text.Image image2 = com.lowagie.text.Image.getInstance(QRCodeCreator.genQRCode(arg));
    image2.scaleAbsolute(120f, 120f);
    document.add(image2);
	document.close();
		}catch(Exception e) {
			e.printStackTrace();
		}
    
    responseOutputStream.flush();

   
    responseOutputStream.close();

    facesContext.responseComplete(); //Important! Otherwise JSF will attempt to render the response which obviously will fail since it's already written with a file and closed.
}

}
