package ctr;

import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

import ejb.TenantEjb;
import entity.Tenant;

@ManagedBean
@SessionScoped
public class Tenantctr{
	
	@ManagedProperty(value="#{identity}")
	Identity identity;
	
	
	public Identity getIdentity() {
		return identity;
	}


	public void setIdentity(Identity identity) {
		this.identity = identity;
	}


	@EJB
	TenantEjb tenantejb;
	
	List<Tenant> tenants ;
	

	
	
	public List<Tenant> getAllTenants() {
	return tenants;	
	}
}