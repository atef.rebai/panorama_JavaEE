package filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ctr.Identity;

@WebFilter("/views/*")
//must use a directory to apply filter on it
public class AuthFilter implements Filter{

	@Override
	public void destroy() {
	}

	@Override
	public void doFilter(ServletRequest arg0, ServletResponse arg1, FilterChain filterChain)
			throws IOException, ServletException {
		
		HttpServletRequest request = (HttpServletRequest) arg0;
		HttpServletResponse response = (HttpServletResponse) arg1;
		Identity identity = (Identity) request.getSession().getAttribute("identity");
		
		//if we dont have identity redirect to login page
		 if(identity == null )
			response.sendRedirect(request.getContextPath() + "/login.xhtml");
		
		//if he did not authenticate redirect to login page
		else if(identity.getToken()==null)
			response.sendRedirect(request.getContextPath() + "/login.xhtml");
		
		//else make him pass
		else
			filterChain.doFilter(request, response);
		
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		// TODO Auto-generated method stub
		
	}

}
