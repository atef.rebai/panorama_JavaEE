package utils;

import java.io.FileOutputStream;
import java.io.OutputStream;

import com.lowagie.text.Document;
import com.lowagie.text.PageSize;
import com.lowagie.text.pdf.PdfWriter;

public class PDFCreator {
	public static void genPDF(byte[] imageInByte,OutputStream output) {
		try {
		Document document = new Document(PageSize.A4, 50, 50, 50, 50);
		PdfWriter writer = PdfWriter.getInstance(document,output);
		document.open();
		com.lowagie.text.Image image2 = com.lowagie.text.Image.getInstance(imageInByte);
    image2.scaleAbsolute(120f, 120f);
    document.add(image2);
	document.close();
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
}
