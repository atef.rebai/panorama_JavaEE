package entity;

import java.util.List;

public class Lesson {
	 private int id ;
     private String titre ;
     private String uriImage ;
     private String uriVideo ;

     //nav prop  
      private List<Quiz> quizs ;
     private  List<Certificate> certificates ;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitre() {
		return titre;
	}
	public void setTitre(String titre) {
		this.titre = titre;
	}
	public String getUriImage() {
		return uriImage;
	}
	public void setUriImage(String uriImage) {
		this.uriImage = uriImage;
	}
	public String getUriVideo() {
		return uriVideo;
	}
	public void setUriVideo(String uriVideo) {
		this.uriVideo = uriVideo;
	}
	public List<Quiz> getQuizs() {
		return quizs;
	}
	public void setQuizs(List<Quiz> quizs) {
		this.quizs = quizs;
	}
	public List<Certificate> getCertificates() {
		return certificates;
	}
	public void setCertificates(List<Certificate> certificates) {
		this.certificates = certificates;
	}




}
