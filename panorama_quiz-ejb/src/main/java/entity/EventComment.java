package entity;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

public class EventComment {

	@JsonProperty("Id")
	 private int id ;
	@JsonProperty("Content")
	private String content ;
	@JsonProperty("CreationDate")
	private String creationDate ;

     //for key
	@JsonProperty("UserId")
     private int userId ;
	@JsonProperty("EventId")
     private int eventId ;

     //nav prop
	@JsonProperty("Event")
     private  Event event ;
	@JsonProperty("User")
     private  User user ;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public int getEventId() {
		return eventId;
	}
	public void setEventId(int eventId) {
		this.eventId = eventId;
	}
	public Event getEvent() {
		return event;
	}
	public void setEvent(Event event) {
		this.event = event;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
     
     
     
}
