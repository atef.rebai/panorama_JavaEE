package entity;

public class Quiz extends Post{
	 //private TimeSpan? Duration ;
     private int score ;

     //prop nav
     private  Lesson lesson ;
    

     //for key
     private int lessonId ;


	public int getScore() {
		return score;
	}


	public void setScore(int score) {
		this.score = score;
	}


	public Lesson getLesson() {
		return lesson;
	}


	public void setLesson(Lesson lesson) {
		this.lesson = lesson;
	}


	public int getLessonId() {
		return lessonId;
	}


	public void setLessonId(int lessonId) {
		this.lessonId = lessonId;
	}
     
     
}
