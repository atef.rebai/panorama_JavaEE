package entity;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class User {
	@JsonProperty("Id")
    private int id ;
	@JsonProperty("Name")
    private String name ;
	@JsonProperty("LastName")
    private String lastName ;
	@JsonProperty("Email")
    private String email ;
	@JsonProperty("username")
    private String username ;
	@JsonProperty("Password")
    private String password ;
	@JsonProperty("ProfileImageURL")
    private String profileImageURL ;
	@JsonProperty("State")
    private String state ;
	@JsonProperty("Role")
    private String role ;
	@JsonProperty("Sexe")
	private String sexe ;
    

	// cle etr
	@JsonProperty("TenantId")
    private int tenantId ;


    //navigation prop
	@JsonProperty("Tenant")
     private Tenant tenant ;
	@JsonProperty("CreatedPosts")
     private List<Post> createdPosts ;
	@JsonProperty("AuthorizedPosts")
    private List<Post> authorizedPosts ;
	@JsonProperty("Topics")
    private  List<Topic> topics ;
	@JsonProperty("Comments")
    private  List<Comment> comments ;
	@JsonProperty("Answers")
    private  List<Answer> answers ;
	@JsonProperty("Envents")
    private  List<Event> events ;
	@JsonProperty("EventComments")
    private  List<EventComment> eventComments ;
	@JsonProperty("Certificates")
    private  List<Certificate> certificates ;
	@JsonProperty("EventParticipations")
	private List eventParticipations ;

	public List getEventParticipations() {
		return eventParticipations;
	}

	public void setEventParticipations(List eventParticipations) {
		this.eventParticipations = eventParticipations;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getProfileImageURL() {
		return profileImageURL;
	}

	public void setProfileImageURL(String profileImageURL) {
		this.profileImageURL = profileImageURL;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}
	public String getSexe() {
		return sexe;
	}

	public void setSexe(String sexe) {
		this.sexe = sexe;
	}
	public int getTenantId() {
		return tenantId;
	}

	public void setTenantId(int tenantId) {
		this.tenantId = tenantId;
	}

	public Tenant getTenant() {
		return tenant;
	}

	public void setTenant(Tenant tenant) {
		this.tenant = tenant;
	}

	public List<Post> getCreatedPosts() {
		return createdPosts;
	}

	public void setCreatedPosts(List<Post> createdPosts) {
		this.createdPosts = createdPosts;
	}

	public List<Post> getAuthorizedPosts() {
		return authorizedPosts;
	}

	public void setAuthorizedPosts(List<Post> authorizedPosts) {
		this.authorizedPosts = authorizedPosts;
	}

	public List<Topic> getTopics() {
		return topics;
	}

	public void setTopics(List<Topic> topics) {
		this.topics = topics;
	}

	public List<Comment> getComments() {
		return comments;
	}

	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}

	public List<Answer> getAnswers() {
		return answers;
	}

	public void setAnswers(List<Answer> answers) {
		this.answers = answers;
	}

	public List<Event> getEvents() {
		return events;
	}

	public void setEvents(List<Event> events) {
		this.events = events;
	}

	public List<EventComment> getEventComments() {
		return eventComments;
	}

	public void setEventComments(List<EventComment> eventComments) {
		this.eventComments = eventComments;
	}

	public List<Certificate> getCertificates() {
		return certificates;
	}

	public void setCertificates(List<Certificate> certificates) {
		this.certificates = certificates;
	}
    
    
    
    
    
    
    
}
