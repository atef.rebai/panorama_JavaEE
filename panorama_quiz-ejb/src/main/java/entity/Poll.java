package entity;

import java.util.Date;

public class Poll extends Post
{
	private Date endDate ;
    private String imageURI ;
    private boolean isActive;
    
    
    //prop de navig
    private  Event event ;


	public Date getEndDate() {
		return endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}


	public String getImageURI() {
		return imageURI;
	}


	public void setImageURI(String imageURI) {
		this.imageURI = imageURI;
	}


	public boolean isActive() {
		return isActive;
	}


	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}


	public Event getEvent() {
		return event;
	}


	public void setEvent(Event event) {
		this.event = event;
	}
    
    
    
}
