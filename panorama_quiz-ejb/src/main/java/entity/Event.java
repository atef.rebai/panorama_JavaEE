package entity;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;



public class Event {

	@JsonProperty("Id")
	private int id ;
	@JsonProperty("Title")
    private String title ;
	@JsonProperty("ImageURI")
    private String  imageURI ;
	@JsonProperty("Content")
    private String content ;
	@JsonProperty("DateCreation")
    private Date dateCreation ;
	@JsonProperty("EndDate")
    private Date endDate ;
	@JsonProperty("Latitude")
    private float latitude ;
	@JsonProperty("Longitude")
    private float longitude ;
	@JsonProperty("Location")
    private String location ;
	@JsonProperty("IsActive")
    private boolean isActive;
	@JsonProperty("NbPlaces")
	private int nbPlaces;
	
    
    // cles etrangers 
	@JsonProperty("UserId")
    private int userId ;
    
    //prop de navig
	@JsonProperty("User")
    private  User user ;
	@JsonProperty("Poll")
    private  Poll poll ;
	@JsonProperty("EventComments")
    private  List<EventComment> eventComments ;
	@JsonProperty("ParticipatingUsers")
	private List<EventParticipation> participatingUsers ;
    
    public List<EventParticipation> getParticipatingUsers() {
		return participatingUsers;
	}
	public void setParticipatingUsers(List<EventParticipation> participatingUsers) {
		this.participatingUsers = participatingUsers;
	}
	@JsonProperty("Id")
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getImageURI() {
		return imageURI;
	}
	public void setImageURI(String imageURI) {
		this.imageURI = imageURI;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public Date getDateCreation() {
		return dateCreation;
	}
	public void setDateCreation(Date dateCreation) {
		this.dateCreation = dateCreation;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public float getLatitude() {
		return latitude;
	}
	public void setLatitude(float latitude) {
		this.latitude = latitude;
	}
	public float getLongitude() {
		return longitude;
	}
	public void setLongitude(float longitude) {
		this.longitude = longitude;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public boolean isActive() {
		return isActive;
	}
	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Poll getPoll() {
		return poll;
	}
	public void setPoll(Poll poll) {
		this.poll = poll;
	}
	public List<EventComment> getEventComments() {
		return eventComments;
	}
	public void setEventComments(List<EventComment> eventComments) {
		this.eventComments = eventComments;
	}
	public int getNbPlaces() {
		return nbPlaces;
	}
	public void setNbPlaces(int nbPlaces) {
		this.nbPlaces = nbPlaces;
	}
	
	
	
}
