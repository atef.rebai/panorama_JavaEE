package entity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

public class EventParticipation {
	
	@JsonProperty("CreationDate")
	private String creationDate ;
	@JsonProperty("State")
	private String state ;
	@JsonProperty("ActivationKey")
	private String activationKey ;

    //keys
	@JsonProperty("UserId")
	private int userId ;
	@JsonProperty("EventId")
	private int eventId ;
    
	//nav prop
	@JsonProperty("User")
	private User user ;
	@JsonProperty("Event")
	private Event event ;
	

	public String getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(String creationDate) {
        this.creationDate=creationDate;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getActivationKey() {
		return activationKey;
	}
	public void setActivationKey(String activationKey) {
		this.activationKey = activationKey;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public int getEventId() {
		return eventId;
	}
	public void setEventId(int eventId) {
		this.eventId = eventId;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Event getEvent() {
		return event;
	}
	public void setEvent(Event event) {
		this.event = event;
	}
	
	
	
	
	
}
