package entity;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Tenant {
	
	
	@JsonProperty("Id")
	 private int id ;
	@JsonProperty("Name")
     private String name ;
	@JsonProperty("LogoURI")
     private String logoURI ;
	@JsonProperty("CustemColor")
     private String custemColor ;
	@JsonProperty("WebSiteURL")
     private String webSiteURL ;

     //navigation prop:
	@JsonProperty("Users")
      private List<User> users ;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLogoURI() {
		return logoURI;
	}

	public void setLogoURI(String logoURI) {
		this.logoURI = logoURI;
	}

	public String getCustemColor() {
		return custemColor;
	}

	public void setCustemColor(String custemColor) {
		this.custemColor = custemColor;
	}

	public String getWebSiteURL() {
		return webSiteURL;
	}

	public void setWebSiteURL(String webSiteURL) {
		this.webSiteURL = webSiteURL;
	}

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}
      
            
}
