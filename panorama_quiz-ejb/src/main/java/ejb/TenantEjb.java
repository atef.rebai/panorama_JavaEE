package ejb;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import entity.Tenant;

/**
 * Session Bean implementation class Tenantservice
 */
@Stateless
@LocalBean
public class TenantEjb implements TenantserviceRemote, TenantserviceLocal {

    /**
     * Default constructor. 
     */Client client;
 	WebTarget target;
 	WebTarget custem_target;  
 	//String token="Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6ImVzcHJpdCIsInJvbGUiOiJBZG1pbiIsIlRlbmFudElkIjoiMSIsIklkIjoiMSIsIlRlbmFudE5hbWUiOiJlc3ByaXQiLCJUZW5hbnRMb2dvVXJpIjoiaW1hZ2UuanBnIiwiVGVuYW50Q29sb3IiOiIjMDAwMENEIiwiVGVuYW50U2l0ZSI6ImVzcHJpdC5jb20iLCJpbWFnZXVyaSI6IjEuanBnIiwibmJmIjoxNTIyNjc3OTI4LCJleHAiOjE1MjUyNjk5MjgsImlhdCI6MTUyMjY3NzkyOH0.keY4oyBmdyOxKVDN2Cii9ir8uI9BrjjuPEWgO_hdaLA";
 	 public TenantEjb() {
         
     	client = ClientBuilder.newClient();
 		target = client.target("http://pi-net-servicepattern-dev.eu-west-1.elasticbeanstalk.com/api/") ; 
     	//target = client.target("http://jsonplaceholder.typicode.com/"); 
     	//target = client.target("http://localhost:11373/api/") ;
 	
 		
 	 }
     
     
     
     public Tenant getById(int id,String token) {
     	
    	 custem_target = target.path("Tenant/"+id);
     	Response res=(Response) custem_target.request(MediaType.APPLICATION_JSON).header("Authorization", token).get();
     	System.out.println(res.getStatusInfo().getStatusCode());
     	//System.out.println(res.readEntity(String.class));
 		 Tenant post = res.readEntity(Tenant.class);
 		 System.out.println("ref"+post);
 		 System.out.println(post.getName());
 		 
 		return post;
 		

     }
     public List <Tenant> getAll (String token) {
   	   System.out.println("les tenants ");
   	custem_target = target.path("Tenant/");
   	  Response res=(Response) custem_target.request(MediaType.APPLICATION_JSON).header("Authorization", token).get();
      List <Tenant> post = res.readEntity(new GenericType<List <Tenant>>() {}) ;
      
      return post ;

 } }

  


