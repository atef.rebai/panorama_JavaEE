package ejb;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.faces.bean.ManagedProperty;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import entity.Event;
import entity.EventComment;
import entity.EventParticipation;
import entity.User;
import utils.MailService;
/**
 * Session Bean implementation class EventBean
 */
@Stateless
@LocalBean
public class EventEjb {

	Client client;
	WebTarget target;
	WebTarget custem_target;
	//static String token="Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6ImVzcHJpdCIsInJvbGUiOiJBZG1pbiIsIlRlbmFudElkIjoiMSIsIklkIjoiMSIsIlRlbmFudE5hbWUiOiJlc3ByaXQiLCJUZW5hbnRMb2dvVXJpIjoiaW1hZ2UuanBnIiwiVGVuYW50Q29sb3IiOiIjMDAwMENEIiwiVGVuYW50U2l0ZSI6ImVzcHJpdC5jb20iLCJpbWFnZXVyaSI6IjEuanBnIiwibmJmIjoxNTIyNjc3OTI4LCJleHAiOjE1MjUyNjk5MjgsImlhdCI6MTUyMjY3NzkyOH0.keY4oyBmdyOxKVDN2Cii9ir8uI9BrjjuPEWgO_hdaLA";
    public EventEjb() {
        
    	client = ClientBuilder.newClient();
    	
    		//target = client.target("http://localhost:11382/api/");
		target = client.target("http://pi-net-servicepattern-dev.eu-west-1.elasticbeanstalk.com/api/"); 	
    }
    
    
    public Event getById(int eventId,String token) {
    	System.out.println("***** get one event *****");
    	System.out.println(eventId);
    	System.out.println(token);
    	custem_target = target.path("event/"+eventId);
    	Response res=(Response) custem_target.request(MediaType.APPLICATION_JSON).header("Authorization",token).get();
    	System.out.println(res.getStatusInfo().getStatusCode());
    	//System.out.println(res.readEntity(String.class));
		 Event post = res.readEntity(Event.class);
		 System.out.println("event consumed: "+post.getId());
		 
		 //setting comments for one event in datails
		 custem_target = target.path("eventComment/");
		 res=(Response) custem_target.request(MediaType.APPLICATION_JSON).header("Authorization",token).get();
		  
		 List<EventComment> eventComment = res.readEntity(new GenericType<List<EventComment>>() {});
		 System.out.println("eventcomments consumed: "+eventComment.size());
		 eventComment.removeIf(s->s.getEventId()!= post.getId());
		 System.out.println("eventcomments after removal: "+eventComment.size());
		 post.setEventComments(eventComment);
		 
		 //setting participations in one event in details
		 custem_target = target.path("eventparticipation/"+post.getId());
    	 res=(Response) custem_target.request(MediaType.APPLICATION_JSON).header("Authorization", token).get();
    	List<EventParticipation> eventParticipations = res.readEntity(new GenericType<List<EventParticipation>>() {});
    	System.out.println("eventParticipation consumed: "+eventParticipations.size());
    	post.setParticipatingUsers(eventParticipations);
    	System.out.println("event "+post.getId()+" has "+post.getParticipatingUsers().size()+" participations");
		 
		return post;

    }
    
    
    public List<Event> getAll(String token) {
    	System.out.println("***** get all events *****");
    	System.out.println("token: "+token);
    	custem_target = target.path("event");
    	Response res=(Response) custem_target.request(MediaType.APPLICATION_JSON).header("Authorization", token).get();
    	//System.out.println(res.readEntity(String.class));
		List<Event> post = res.readEntity(new GenericType<List<Event>>() {});
		for(Event e : post) {
			custem_target = target.path("eventparticipation/"+e.getId());
	    	 res=(Response) custem_target.request(MediaType.APPLICATION_JSON).header("Authorization",token).get();
	    	List<EventParticipation> eventParticipations = res.readEntity(new GenericType<List<EventParticipation>>() {});
	    	e.setParticipatingUsers(eventParticipations);
	    	System.out.println("event "+e.getId()+" has "+e.getParticipatingUsers().size()+" participations");
			}
		
		 System.out.println("size of list is : "+post.size());
		return post;
    }
    
    public List<Event> getBySearchString(String searchString,String token){
    	List<Event> searchedEvents=this.getAll(token);
    	searchedEvents.removeIf(x-> !(x.getLocation().toLowerCase().contains(searchString.toLowerCase())) && !(x.getTitle().toLowerCase().contains(searchString.toLowerCase())) );
    	return searchedEvents;
    }
    
    /*
    public String AddEvent(Event e) {
    	custem_target = target.path("event/");
    	Event return_event = custem_target.request(MediaType.APPLICATION_JSON).header("Authorization", token).post(Entity.entity(e, MediaType.APPLICATION_JSON), Event.class);
		System.out.println("returned event"+return_event.getId());
		return return_event.getTitle();
    }
    */
    
    public List<EventParticipation> getSubs(int eventId,String token) {
    	System.out.println("***** get subs *****");
    	//get subs for event 'id'
    	custem_target = target.path("EventParticipation/"+eventId);
    	Response res=(Response) custem_target.request(MediaType.APPLICATION_JSON).header("Authorization", token).get();
		 List<EventParticipation> response_object = res.readEntity(new GenericType<List<EventParticipation>>() {});
		 System.out.println("res:"+response_object);
    	return response_object;
    }
    
    public EventParticipation subscribe(int eventId,String email,String token) {
    	try {
    	System.out.println("***** sybscribe *****");
    	custem_target = target.path("EventParticipation/"+eventId);
    	EventParticipation return_event = custem_target.request(MediaType.APPLICATION_JSON).header("Authorization", token).post(Entity.entity(new EventParticipation(), MediaType.APPLICATION_JSON), EventParticipation.class);
    	
    	this.sendMail(email, "http://pi-net-servicepattern-dev.eu-west-1.elasticbeanstalk.com/api/ep/confirm/"+return_event.getActivationKey());
    	
    	System.out.println("response "+return_event);
    	System.out.println("returned event"+return_event.getEventId()+":"+return_event.getUserId());
    	System.out.println("returned event date"+return_event.getCreationDate().toString());
		return return_event;
    	}catch(Exception e) {
    		System.out.println("err subscribe");
    		return null;
    	}
    	
    }
    
    public void sendMail(String email,String validation_URI) {
    	try {
    	MailService mailservice=new MailService();
    	mailservice.sendEMail(email, "confirm your participation",validation_URI);
    	}catch(Exception e) {
    		System.out.println("err sending mail");
    	}
    }
    
    public void addComment(EventComment com,String token) {
    	custem_target = target.path("eventcomment/");
    	EventComment return_event = custem_target.request(MediaType.APPLICATION_JSON).header("Authorization", token).post(Entity.entity(com, MediaType.APPLICATION_JSON), EventComment.class);
		System.out.println("returned eventComment"+return_event.getId());
		
    }
    
   
    
    
   
    /*
     * 
     * this method was added by safa elmi
     * 
     */
    public List<Event> getParticipatedEvents(User user,String token){
    	List<Event> allEvents  = getAll(token)  ; 
    	List<Event> participatedEvents  = new ArrayList<>()  ;  
    	  allEvents.forEach(item  ->  {
    		  boolean participate  = true ; 
      		for(EventParticipation eventPart : item.getParticipatingUsers()) {
      			if(eventPart.getUserId()==user.getId()) {
      				participate=false ; 
      				break ; 
      			}
      		}
      		if(participate) {
      			participatedEvents.add(item)  ; 
      		}
    	  });
    	  return participatedEvents ; 
    }

    
    /*
     * 
     * this method was added by safa elmi
     * 
     */    
    public  List<Event> getEventsByDate(User user,String token){
    	List<Event>  allEvents  =  getAll(token)  ; 
    	List<Event> futureEvents  = new ArrayList<>()  ;  
    	allEvents.forEach(item  ->  {
    		boolean participate  = true ; 
    		for(EventParticipation eventPart : item.getParticipatingUsers()) {
    			if(eventPart.getUserId()==user.getId()) {
    				participate=false ; 
    				break ; 
    			}
    		}
    		if(item.getDateCreation().after(new Date())&&(participate==true)) {
    			futureEvents.add(item)  ; 
    		}
    	});
    	return futureEvents ; 
    	
    }
    
    
    
    /*
     * 
     * this method was added by safa elmi
     * 
     */
    public List<Event> getMostInterrestedEvents(String token){
    	List<Event> allEvents = getAll(token)  ;
    	List<Event> bestEvents  = new ArrayList<>()  ;  
    	allEvents.sort(new Comparator<Event>() {

			@Override
			public int compare(Event o1, Event o2) {
				// TODO Auto-generated method stub
				return new Integer(o1.getParticipatingUsers().size()).compareTo(new Integer(o2.getParticipatingUsers().size()));
			}
		});
    	for(int i  =0  ; i<10  ; i++) {
    		bestEvents.add(allEvents.get(i)) ; 
    	}
    	return bestEvents  ;  
    }
    
    

}
