package ejb;

import java.util.List;

import utils.MailService;
import utils.Token;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.persistence.Query;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import entity.Event;
import entity.User;

/**
 * Session Bean implementation class UserBean
 */
@Stateless
@LocalBean
public class UserEjb{
	 
	Client client;
	WebTarget target;
	WebTarget custem_target;
	//String token="Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6ImVzcHJpdCIsInJvbGUiOiJBZG1pbiIsIlRlbmFudElkIjoiMSIsIklkIjoiMSIsIlRlbmFudE5hbWUiOiJlc3ByaXQiLCJUZW5hbnRMb2dvVXJpIjoiaW1hZ2UuanBnIiwiVGVuYW50Q29sb3IiOiIjMDAwMENEIiwiVGVuYW50U2l0ZSI6ImVzcHJpdC5jb20iLCJpbWFnZXVyaSI6IjEuanBnIiwibmJmIjoxNTIyNjc3OTI4LCJleHAiOjE1MjUyNjk5MjgsImlhdCI6MTUyMjY3NzkyOH0.keY4oyBmdyOxKVDN2Cii9ir8uI9BrjjuPEWgO_hdaLA";
    public UserEjb() {
        
    	client = ClientBuilder.newClient();
		target = client.target("http://pi-net-servicepattern-dev.eu-west-1.elasticbeanstalk.com/api/"); 	
    }
    

    
    
    public String getById(int id,String token) {
    	custem_target = target.path("User/"+id);
    	Response res=(Response) custem_target.request(MediaType.APPLICATION_JSON).header("Authorization", token).get();
    	System.out.println(res.getStatusInfo().getStatusCode());
    	//System.out.println(res.readEntity(String.class));
		 User post = res.readEntity(User.class);
		 System.out.println("ref"+post);
		 System.out.println(post.getUsername());
		 
		return post.getUsername();

    } 
   
    public List <User> getAll (String token) {
 	   System.out.println("les users ");
 	   target = target.path("user/");
 	  Response res=(Response) target.request(MediaType.APPLICATION_JSON).header("Authorization", token).get();
    List <User> post = res.readEntity(new GenericType<List <User>>() {}) ;
    
    return post ; 
    }
    
    public String AddUser(User u,String token) {
    	custem_target = target.path("user/");
    	u.setState("inactive");
    	u.setRole("user");
    	User return_user = custem_target.request(MediaType.APPLICATION_JSON).header("Authorization", token).post(Entity.entity(u, MediaType.APPLICATION_JSON), User.class);
		System.out.println("returned user"+return_user.getId());
		MailService mail=new MailService();
		mail.sendEMail(u.getEmail(), "Registre", "wait for Admin to confirm your account");
		return return_user.getUsername();
		
    }

    public Response update( User u, int id,String token) 
	{
		
    	custem_target = target.path("user/"+id);
		Response res=(Response) custem_target.request(MediaType.APPLICATION_JSON).header("Authorization", token).put(Entity.json(u));
    	System.out.println("status code = "+res.getStatus());
    	System.out.println(u.toString());
    	
    	return res;
	}
    
    public Response delete(int id,String token) 
	{
		
    	custem_target = target.path("user/"+id);
		Response res=(Response) custem_target.request(MediaType.APPLICATION_JSON).header("Authorization", token).delete();
		
		System.out.println(res.getStatusInfo().getStatusCode());
	//System.out.println(res.readEntity(String.class));
	 
	 
	return res;
	}
   
    public Token logIn(User user) {
    	
    	custem_target = target.path("auth/simpleauth/");
    	Token response = custem_target.request(MediaType.APPLICATION_JSON).post(Entity.entity(user, MediaType.APPLICATION_JSON), Token.class);
		return response;
	
    }
    
    public User getByToken(Token token) {
    	custem_target = target.path("auth/");
    	Response res=(Response) custem_target.request(MediaType.APPLICATION_JSON).header("Authorization","Bearer "+token.getToken()).get();
    	User user = res.readEntity(User.class);
		return user;
    }

}

